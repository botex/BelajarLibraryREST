package gulajava.teslibraryrests.internet.loopje;

import com.loopj.android.http.AsyncHttpClient;

/**
 * Created by Gulajava Ministudio on 2/2/16.
 */
public class LoopJSingleton {

    private static LoopJSingleton ourInstance;

    private AsyncHttpClient mAsyncHttpClient;

    public static LoopJSingleton getInstance() {

        if (ourInstance == null) {
            ourInstance = new LoopJSingleton();
        }

        return ourInstance;
    }

    private LoopJSingleton() {

        mAsyncHttpClient = new AsyncHttpClient();
        mAsyncHttpClient.addHeader("Content-Type", "application/json");
        mAsyncHttpClient.setConnectTimeout(10000);

    }

    public AsyncHttpClient getAsyncHttpClient() {
        return mAsyncHttpClient;
    }
}
