package gulajava.teslibraryrests.internet.retrofits1;

import gulajava.teslibraryrests.modelan.KandidatModel;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public interface Apis {


    //http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=8e67d2396454f98c370596b139194015&limit=100
    @GET("/calonpilkada/api/candidates")
    void getDaftarKandidat(@Query("apiKey") String apikey, @Query("limit") String limitdata,
                           Callback<KandidatModel> callback);


}
